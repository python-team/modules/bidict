bidict (0.23.1-2) unstable; urgency=medium

  * Team upload.
  * d/tests/control: Add python3-typing-extensions as Dependency.

 -- Emmanuel Arias <eamanu@debian.org>  Sun, 21 Apr 2024 20:42:31 -0300

bidict (0.23.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * d/patches: Update 0002-Handle-privacy-breach-in-docs.patch and
    0004-drop-intersphinx.patch patches according to the new upstream code.
    - Remove 0003-Declare-packages-in-setup.cfg.patch and 0005-python3.12.patch
      patches, they are not longer needed.
  * d/control: Add python3-typing-extensions as B-Depends.

 -- Emmanuel Arias <eamanu@debian.org>  Sun, 21 Apr 2024 14:24:12 -0300

bidict (0.22.1-2) unstable; urgency=medium

  * Team upload.
  * Add patch to support Python 3.12 Closes: #1056230
  * Make build reproducible.
    Thanks to Chris Lamb for patch Closes: #1055920
  * Update patch to handle privacy breach.
    - Rename patch to '0002-Handle-privacy-breach-in-docs.patch'.
    - Remove 'docs/_static/custom.js'.
 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Mon, 27 Nov 2023 11:37:22 +0100

bidict (0.22.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Refresh patches.
  * Drop patch 0003-Do-not-install-custom.js-for-codefund.app-because-of.patch,
    superseded upstream.
  * Switch watch file to github tags.
  * Patch: Help setuptools find the package.
  * Build-Depend on python3-pytest-xdist.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 03 Feb 2023 19:50:55 -0400

bidict (0.22.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Patches:
    - Rebase patches.
    - Drop 0001-Disable-alabaster-theme-and-some-options-to-avoid-pr.patch
  * Mark BD's related to tests with <!nocheck>.
  * Add python3-sphinx-copybutton and furo as BD for documentation, and
    pybuild-plugin-pyproject.
  * Update Standards-Version to 4.6.2, no changes needed.
  * Split out documentation to a separate package.
  * d/copyright:
    - Update file location for upstream logo.
    - Update copyright year.
  * Run wrap-and-sort -at
  * Add upstream testsuite as autopkgtest.
  * Disable benchmarks when running testsuite.

 -- Håvard F. Aasen <havard.f.aasen@pfft.no>  Sun, 08 Jan 2023 17:02:32 +0100

bidict (0.21.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.21.3
  * Rebase patches.
  * d/control:
    - Drop obsolete build-dependency.
    - Update Standards-Version to 4.6.0
  * d/copyright: Update copyright year.
  * d/clean: Update file.

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Tue, 21 Sep 2021 03:48:30 +0200

bidict (0.21.2-2) unstable; urgency=medium

  * Team Upload.
  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Håvard Flaget Aasen ]
  * Remove regenerated file by scm. Closes: #978282
  * New upstream version 0.21.2
  * d/patches:
    - Rebase patches
    - Add 0004-drop-intersphinx.patch
  * d/control: Update Standards-Version to 4.5.1
  * d/copyright:
    - Add Upstream-Contact
    - Update copyright holders year and email
  * d/gbp.conf: New file, change default branch to debian/master

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 27 Dec 2020 16:48:26 +0100

bidict (0.21.0-1) unstable; urgency=medium

  * New upstream version 0.21.0 Closes: #963380
  * Rebase patches
  * d/control:
    - Add python3-sphinx-autodoc-typehints as Build-Depends
    - Add Rules-Requires-Root: no
    - Bump debhelper to 13
    - Update Standards-Version to 4.5.0
    - Change to Debian Python Team as maintainer
  * d/*.links: add entry to link duplicated file

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Fri, 04 Sep 2020 11:22:11 +0200

bidict (0.18.2-1) unstable; urgency=medium

  * New upstream release
  * d/control: remove line about compatible python versions from description
  * d/patches: reapply patch 0002 catching up upstream changes

 -- William Grzybowski <william@grzy.org>  Tue, 17 Sep 2019 13:41:16 -0300

bidict (0.18.0-2) unstable; urgency=medium

  * Team upload.
  * Uploading to unstable.

 -- Ondřej Nový <onovy@debian.org>  Wed, 14 Aug 2019 17:59:48 +0200

bidict (0.18.0-1) experimental; urgency=medium

  * Initial release (Closes: #932216)

 -- William Grzybowski <william@grzy.org>  Wed, 14 Aug 2019 13:06:09 +0200
